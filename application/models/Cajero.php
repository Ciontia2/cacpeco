<?php
  class Cajero extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos sucursales
    function insertar($datos){
      $respuesta=$this->db->insert("cajero",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $cajeros=$this->db->get("cajero");
      if ($cajeros->num_rows()>0) {
        return $cajeros->result();
      } else {
        return false;
      }
    }
    //eliminacion de sucursal por id
    function eliminar($id){
        $this->db->where("id_ca",$id);
        return $this->db->delete("cajero");
    }
    //Consulta de un solo sucursal
  function obtenerPorId($id){
    $this->db->where("id_ca",$id);
    $cajero=$this->db->get("cajero");
    if ($cajero->num_rows()>0) {
      return $cajero->row();
    } else {
      return false;
    }
  }
  //funcion para actualizar sucursal
      function actualizar($id,$datos){
        $this->db->where("id_ca",$id);
        return $this->db
                    ->update("cajero",$datos);
      }

  }//Fin de la clase
?>
