<?php
  class Sucursal extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos sucursales
    function insertar($datos){
      $respuesta=$this->db->insert("sucursal",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $sucursales=$this->db->get("sucursal");
      if ($sucursales->num_rows()>0) {
        return $sucursales->result();
      } else {
        return false;
      }
    }
    //eliminacion de sucursal por id
    function eliminar($id){
        $this->db->where("id_su",$id);
        return $this->db->delete("sucursal");
    }
    //Consulta de un solo sucursal
  function obtenerPorId($id){
    $this->db->where("id_su",$id);
    $sucursal=$this->db->get("sucursal");
    if ($sucursal->num_rows()>0) {
      return $sucursal->row();
    } else {
      return false;
    }
  }
  //funcion para actualizar sucursal
      function actualizar($id,$datos){
        $this->db->where("id_su",$id);
        return $this->db
                    ->update("sucursal",$datos);
      }

  }//Fin de la clase
?>
