<?php
class Ubicacion extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function obtenerSucursales()
    {
        $listadoSucursales = $this->db->select('*')->from('sucursal')->get();
        if ($listadoSucursales->num_rows() > 0) {
            return $listadoSucursales->result();
        }
        return false;
    }

    function obtenerCajeros()
    {
        $listadoCajeros = $this->db->select('*')->from('cajero')->get();
        if ($listadoCajeros->num_rows() > 0) {
            return $listadoCajeros->result();
        }
        return false;
    }

    function obtenerCorresponsales()
    {
        $listadoCorresponsales = $this->db->select('*')->from('corresponsal')->get();
        if ($listadoCorresponsales->num_rows() > 0) {
            return $listadoCorresponsales->result();
        }
        return false;
    }
}
?>
