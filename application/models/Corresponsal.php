<?php
  class Corresponsal extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos corresponsales
    function insertar($datos){
      $respuesta=$this->db->insert("corresponsal",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $corresponsales=$this->db->get("corresponsal");
      if ($corresponsales->num_rows()>0) {
        return $corresponsales->result();
      } else {
        return false;
      }
    }
    //eliminacion de corresponsal por id
    function eliminar($id){
        $this->db->where("id_co",$id);
        return $this->db->delete("corresponsal");
    }
    //Consulta de un solo corresponsal
  function obtenerPorId($id){
    $this->db->where("id_co",$id);
    $corresponsal=$this->db->get("corresponsal");
    if ($corresponsal->num_rows()>0) {
      return $corresponsal->row();
    } else {
      return false;
    }
  }
  //funcion para actualizar corresponsal
      function actualizar($id,$datos){
        $this->db->where("id_co",$id);
        return $this->db
                    ->update("corresponsal",$datos);
      }

  }//Fin de la clase
?>
