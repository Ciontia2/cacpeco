<?php
class Ubicaciones extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Ubicacion");
    }

    public function sucursal()
    {
        $data["sucursales"] = $this->Ubicacion->obtenerSucursales();
        $this->cargarVista('sucursales', $data);
    }

    public function cajero()
    {
        $data["cajeros"] = $this->Ubicacion->obtenerCajeros();
        $this->cargarVista('cajeros', $data);
    }

    public function corresponsal()
    {
        $data["corresponsales"] = $this->Ubicacion->obtenerCorresponsales();
        $this->cargarVista('corresponsales', $data);
    }

    public function ubicaciones_general()
    {
        $data["sucursales"] = $this->Ubicacion->obtenerSucursales();
        $data["cajeros"] = $this->Ubicacion->obtenerCajeros();
        $data["corresponsales"] = $this->Ubicacion->obtenerCorresponsales();

        $this->load->view('header');
        $this->load->view('ubicaciones/ubicaciones_general', $data);
        $this->load->view('footer');
    }

    private function cargarVista($vista, $data)
    {
        $this->load->view('header');
        $this->load->view("ubicaciones/{$vista}", $data);
        $this->load->view('footer');
    }
}
?>
