<?php
  class Sucursales extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Sucursal");
    }
    //Renderizacion de la vista index de SUCURSALES
    public function index(){
      $data["listadoSucursales"]=$this->Sucursal->consultarTodos();
      $this->load->view("header");
      $this->load->view("sucursales/index",$data);
      $this->load->view("footer");
    }
    //eliminacion de sucursales recibiendo el id por get
    public function borrar($id_su){
      $this->Sucursal->eliminar($id_su);
      $this->session->set_flashdata("confirmacion","Sucursal eliminado exitosamente");
      redirect("sucursales/index");
    }
    //renderizacion de formulario nuevo sucursales
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("sucursales/nuevo");
      $this->load->view("footer");
    }
    //cpturando datos e insertando en sucursales
    public function guardarSucursal(){
    // INICIO PROCESO DE SUBIDA DE ARCHIVO
    $config['upload_path'] = APPPATH . '../uploads/sucursales/';
    $config['allowed_types'] = 'jpeg|jpg|png';
    $config['max_size'] = 5 * 1024;
    $nombre_aleatorio = "sucursal_" . time() * rand(100, 10000);
    $config['file_name'] = $nombre_aleatorio;

    $this->load->library('upload', $config);

    // Intentando subir el archivo
    if ($this->upload->do_upload("foto_su")) {
        $dataArchivoSubido = $this->upload->data();
        $nombre_archivo_subido = $dataArchivoSubido["file_name"];
        echo "Nombre del archivo subido: " . $nombre_archivo_subido;
    } else {
        $nombre_archivo_subido = "";
        echo "Error al subir el archivo: " . $this->upload->display_errors();
    }

    // Resto del código
    $datosNuevoSucursal = array(
        "nombre_su" => $this->input->post('nombre_su'),
        "telefono_su" => $this->input->post('telefono_su'),
        "gerente_su" => $this->input->post('gerente_su'),
        "fechaapertura_su" => $this->input->post('fechaapertura_su'),
        "latitud_su" => $this->input->post('latitud_su'),
        "longitud_su" => $this->input->post('longitud_su'),
        "foto_su" => $nombre_archivo_subido
    );

    print_r($datosNuevoSucursal);  // Agregamos esta línea para imprimir los datos del formulario

    $this->Sucursal->insertar($datosNuevoSucursal);

    enviarEmail("cintia.ganchala4749@utc.edu.ec", "CREACION", "<h1>SE CREO LA SUCURSAL </h1>" . $datosNuevoSucursal['nombre_su']);

    //flash_data crea una sesión de tipo flash
    $this->session->set_flashdata("confirmacion", "Sucursal guardada exitosamente");
    redirect('sucursales/index');
}


    //Renderizar el formulario de edicion
    public function editar($id){
      $data["sucursalEditar"]=$this->Sucursal->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("sucursales/editar",$data);
      $this->load->view("footer");
    }
    public function actualizarSucursal(){
      $id_su = $this->input->post("id_su");

      // Obtén el nombre del archivo subido de manera similar a la función guardarSucursal
      $config['upload_path'] = APPPATH . '../uploads/sucursales/';
      $config['allowed_types'] = 'jpeg|jpg|png';
      $config['max_size'] = 5 * 1024;
      $nombre_aleatorio = "sucursal_" . time() * rand(100, 10000);
      $config['file_name'] = $nombre_aleatorio;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload("foto_su")) {
          $dataArchivoSubido = $this->upload->data();
          $nombre_archivo_subido = $dataArchivoSubido["file_name"];
          echo "Nombre del archivo subido: " . $nombre_archivo_subido;
      } else {
          $nombre_archivo_subido = "foto_su";
          echo "Error al subir el archivo: " . $this->upload->display_errors();
      }

      $datosSucursal = array(
          "nombre_su" => $this->input->post('nombre_su'),
          "telefono_su" => $this->input->post('telefono_su'),
          "gerente_su" => $this->input->post('gerente_su'),
          "fechaapertura_su" => $this->input->post('fechaapertura_su'),
          "latitud_su" => $this->input->post('latitud_su'),
          "longitud_su" => $this->input->post('longitud_su'),
          "foto_su" => $nombre_archivo_subido  // Asigna el nombre del archivo subido
      );

      $this->Sucursal->actualizar($id_su, $datosSucursal);
      $this->session->set_flashdata("confirmacion", "Sucursal actualizado exitosamente");
      redirect('sucursales/index');
  }

  }//Cierre de  la clase
?>
