<?php
class Corresponsales extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Corresponsal", "corresponsal");
    }

    // Renderización de la vista index de CORRESPONSALES
    public function index()
    {
        $data["listadoCorresponsales"] = $this->corresponsal->consultarTodos();
        $this->load->view("header");
        $this->load->view("corresponsales/index", $data);
        $this->load->view("footer");
    }

    // Eliminación de corresponsales recibiendo el id por get
    public function borrar($id_co)
    {
        $this->corresponsal->eliminar($id_co);
        $this->session->set_flashdata("confirmacion", "Corresponsal eliminado exitosamente");
        redirect("corresponsales/index");
    }

    // Renderización de formulario nuevo corresponsales
    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("corresponsales/nuevo");
        $this->load->view("footer");
    }

    // Capturando datos e insertando en corresponsales
    public function guardarCorresponsal()
    {
        $datosNuevoCorresponsal = array(
            "nombre_co" => $this->input->post('nombre_co'),
            "direccion_co" => $this->input->post('direccion_co'),
            "tipo_servicio_co" => $this->input->post('tipo_servicio_co'),
            "comision_co" => $this->input->post('comision_co'),
            "latitud_co" => $this->input->post('latitud_co'),
            "longitud_co" => $this->input->post('longitud_co'),
        );

        $this->corresponsal->insertar($datosNuevoCorresponsal);
        enviarEmail("cintia.ganchala4749@utc.edu.ec", "CREACION", "<h1>SE CREO LA CORRESPONSAL </h1>" . $datosNuevoSucursal['nombre_su']);
        $this->session->set_flashdata("confirmacion", "Corresponsal guardada exitosamente");
        redirect('corresponsales/index');
    }

    // Renderizar el formulario de edición
    public function editar($id)
    {
        $data["corresponsalEditar"] = $this->corresponsal->obtenerPorId($id);
        $this->load->view("header");
        $this->load->view("corresponsales/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarCorresponsal()
    {
        $id_co = $this->input->post("id_co");

        $datosCorresponsal = array(
            "nombre_co" => $this->input->post('nombre_co'),
            "direccion_co" => $this->input->post('direccion_co'),
            "tipo_servicio_co" => $this->input->post('tipo_servicio_co'),
            "comision_co" => $this->input->post('comision_co'),
            "latitud_co" => $this->input->post('latitud_co'),
            "longitud_co" => $this->input->post('longitud_co'),
        );

        $this->corresponsal->actualizar($id_co, $datosCorresponsal);
        $this->session->set_flashdata("confirmacion", "CORRESPONSAL actualizado exitosamente");
        redirect('corresponsales/index');
    }
}
?>
