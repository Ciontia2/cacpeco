<?php
  class Cajeros extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model("Cajero");
    }
    //Renderizacion de la vista index de SUCURSALES
    public function index(){
      $data["listadoCajeros"]=$this->Cajero->consultarTodos();
      $this->load->view("header");
      $this->load->view("cajeros/index",$data);
      $this->load->view("footer");
    }
    //eliminacion de sucursales recibiendo el id por get
    public function borrar($id_ca){
      $this->Cajero->eliminar($id_ca);
      $this->session->set_flashdata("confirmacion","Cajero eliminado exitosamente");
      redirect("cajeros/index");
    }
    //renderizacion de formulario nuevo sucursales
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("cajeros/nuevo");
      $this->load->view("footer");
    }
    //cpturando datos e insertando en sucursales
    public function guardarCajero(){
    // INICIO PROCESO DE SUBIDA DE ARCHIVO
    $config['upload_path'] = APPPATH . '../uploads/cajeros/';
    $config['allowed_types'] = 'jpeg|jpg|png';
    $config['max_size'] = 5 * 1024;
    $nombre_aleatorio = "cajero_" . time() * rand(100, 10000);
    $config['file_name'] = $nombre_aleatorio;

    $this->load->library('upload', $config);

    // Intentando subir el archivo
    if ($this->upload->do_upload("foto_ca")) {
        $dataArchivoSubido = $this->upload->data();
        $nombre_archivo_subido = $dataArchivoSubido["file_name"];
        echo "Nombre del archivo subido: " . $nombre_archivo_subido;
    } else {
        $nombre_archivo_subido = "";
        echo "Error al subir el archivo: " . $this->upload->display_errors();
    }

    // Resto del código
    $datosNuevoCajero = array(
        "estado_ca" => $this->input->post('estado_ca'),
        "saldo_actual_ca" => $this->input->post('saldo_actual_ca'),
        "latitud_ca" => $this->input->post('latitud_ca'),
        "longitud_ca" => $this->input->post('longitud_ca'),
        "foto_ca" => $nombre_archivo_subido
    );

    print_r($datosNuevoCajero);  // Agregamos esta línea para imprimir los datos del formulario

    $this->Cajero->insertar($datosNuevoCajero);

    enviarEmail("cintia.ganchala4749@utc.edu.ec", "CREACION", "<h1>SE CREO EL BANCO </h1>" . $datosNuevoCajero['estado_ca']);

    //flash_data crea una sesión de tipo flash
    $this->session->set_flashdata("confirmacion", "Cajero guardada exitosamente");
    redirect('cajeros/index');
}


    //Renderizar el formulario de edicion
    public function editar($id){
      $data["cajeroEditar"]=$this->Cajero->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("cajeros/editar",$data);
      $this->load->view("footer");
    }
    public function actualizarCajero(){
      $id_ca = $this->input->post("id_ca");

      // Obtén el nombre del archivo subido de manera similar a la función guardarSucursal
      $config['upload_path'] = APPPATH . '../uploads/cajeros/';
      $config['allowed_types'] = 'jpeg|jpg|png';
      $config['max_size'] = 5 * 1024;
      $nombre_aleatorio = "cajero_" . time() * rand(100, 10000);
      $config['file_name'] = $nombre_aleatorio;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload("foto_ca")) {
          $dataArchivoSubido = $this->upload->data();
          $nombre_archivo_subido = $dataArchivoSubido["file_name"];
          echo "Nombre del archivo subido: " . $nombre_archivo_subido;
      } else {
          $nombre_archivo_subido = "foto_ca";
          echo "Error al subir el archivo: " . $this->upload->display_errors();
      }

      $datosCajero = array(
        "estado_ca" => $this->input->post('estado_ca'),
        "saldo_actual_ca" => $this->input->post('saldo_actual_ca'),
        "latitud_ca" => $this->input->post('latitud_ca'),
        "longitud_ca" => $this->input->post('longitud_ca'),
        "foto_ca" => $nombre_archivo_subido// Asigna el nombre del archivo subido
      );

      $this->Cajero->actualizar($id_ca, $datosCajero);
      $this->session->set_flashdata("confirmacion", "Cajeros actualizado exitosamente");
      redirect('cajeros/index');
  }

  }//Cierre de  la clase
?>
