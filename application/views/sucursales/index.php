<br><br>
<style>
  .contenedor{
    box-shadow: 0px 0px 5px 1px black;
  }
</style>
<div class="container contenedor">
  <br>
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h1 class="text-center">LISTADO DE SUCURSALES</h1>
      </div>
      <div class="col-md-2 nuevo">
      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver Mapa
      </button>
      </div>
      <div class="col-md-2 nuevo">
        <a href="<?php echo site_url('sucursales/nuevo'); ?>" class="btn btn-outline-success">
            <i class="fa fa-plus-circle fa-1x" ></i>  Agregar Sucursales
            </a>
      </div>
    </div>
  </div>
  <br>
  <br>
  <div class="table-responsive">
    <?php if ($listadoSucursales): ?>
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>NOMBRE SUCURSAL</th>
            <th>TELEFONO</th>
            <th>GERENTE</th>
            <th>FRCHA APERTURA</th>
            <th>FOTO</th>
            <th>LATITUD DE LA SUCURSAL</th>
            <th>LONGITUD DE LA SUCURSAL</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoSucursales as $filaTemporal): ?>
            <tr>
              <td>
                <?php echo $filaTemporal->id_su ?>
              </td>
              <td>
                <?php echo $filaTemporal->nombre_su ?>
              </td>
              <td>
                <?php echo $filaTemporal->telefono_su ?>
              </td>
              <td>
                <?php echo $filaTemporal-> gerente_su?>
              </td>
              <td>
                <?php echo $filaTemporal->fechaapertura_su ?>
              </td>
              <td>
                  <?php if ($filaTemporal->foto_su!=""): ?>
                    <img src="<?php echo base_url('uploads/sucursales/') . $filaTemporal->foto_su; ?>" width="100px" height="100px" alt="">
                  <?php else: ?>
                    N/A
                  <?php endif; ?>
                </td>
              <td>
                <?php echo $filaTemporal-> latitud_su?>
              </td>
              <td>
                <?php echo $filaTemporal-> longitud_su?>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url(); ?>/sucursales/editar/<?php echo $filaTemporal->id_su ?>" title="Editar Sucursal" style="color:blue;">
                    <i class="bi bi-pencil-square"></i>
                </a>
                  &nbsp;&nbsp;&nbsp;
                  <a href="<?php echo site_url(); ?>/sucursales/borrar/<?php echo $filaTemporal->id_su ?>" title="Eliminar Sucursal">
                      <i class="fa fa-trash text-danger"></i>
                  </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <br>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <i class="fa fa-eye"></i>Mapa de Sucursales
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="reporteMapa"
          style="height: 300px; width:100%; border:2px solid black;">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary"
          data-bs-dismiss="modal"> <i class="fa fa-times"></i>Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
      function initMap(){
        var coordenadaCentral=
            new google.maps.LatLng(-0.152948869329262,
              -78.4868431364856);
        var miMapa=new google.maps.Map(
          document.getElementById('reporteMapa'),
          {
            center:coordenadaCentral,
            zoom:8,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
        );
        <?php foreach ($listadoSucursales as $filaTemporal): ?>
        var coordenadaTemporal=
            new google.maps.LatLng(
              <?php echo $filaTemporal->latitud_su; ?>,
              <?php echo $filaTemporal->longitud_su; ?>);
          var marcador=new google.maps.Marker({
            position:coordenadaTemporal,
            map:miMapa,
            title:'<?php echo $filaTemporal->nombre_su; ?>',
            icon:"<?php echo base_url() ?>/assets/images/su.png",
          });
        <?php endforeach; ?>

      }
    </script>
    <?php else: ?>
      <h1 class="text-center">No existen Sucursales registradas</h1>
    <?php endif; ?>
  </div>
</div>
