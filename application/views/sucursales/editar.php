<br>
<style>
  .contenedor{
    box-shadow: 0px 0px 5px 1px black;
  }
</style>
<div class="container contenedor">
  <br>
  <h1 class="text-center">ACTUALIZAR DATOS DE SUCURSAL</h1>
  <br>
  <form class="" action="<?php echo site_url(); ?>/sucursales/actualizarSucursal" method="post"enctype="multipart/form-data">
    <div class="container">
      <div class="row">
          <div class="col-md-6">
            <input type="hidden" name="id_su" id="id_su" value="<?php echo $sucursalEditar->id_su; ?>">
            <label for="">NOMBRE</label>
            <br>
            <input type="text" placeholder="Ingrese nombre" class="form-control" required  name="nombre_su" value="<?php echo $sucursalEditar->nombre_su; ?>" id="nombre_su">
          </div>
          <div class="col-md-6">
            <label for="">TELEFONO</label>
            <br>
            <input type="number" placeholder="Ingrese el telefono" class="form-control" required  name="telefono_su" value="<?php echo $sucursalEditar->telefono_su; ?>" id="telefono_su">
          </div>
      </div>
      <br>
      <div class="row">
          <div class="col-md-6">
            <label for="">GERENTE</label>
            <br>
            <input type="text" placeholder="Ingrese el gerente" class="form-control" required  name="gerente_su" value="<?php echo $sucursalEditar->gerente_su; ?>" id="gerente_su">
          </div>
          <div class="col-md-6">
            <label for="">Fecha apertura</label>
            <br>
            <input type="date" placeholder="Ingrese la fecha de apertura" class="form-control" required  name="fechaapertura_su" value="<?php echo $sucursalEditar->fechaapertura_su; ?>" id="fechaapertura_su">
          </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <label for="foto_su" style="display: block; margin-bottom: 10px;"><b>FOTO:</b></label>
          <input type="file" name="foto_su" id="foto_su" value="" required accept="image/*" value="<?php echo $sucursalEditar->foto_ca; ?>" style="width: 100%; padding: 8px;">
               <?php if (!empty($sucursalEditar->foto_su)): ?>
                  <br>
                  <img src="<?php echo base_url('uploads/sucursales/') . $sucursalEditar->foto_su; ?>" height="100px" alt="foto asctual">
               <?php endif; ?>
               <br>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-6">
          <label for="">Latitud de la sucursal</label>
          <input type="text" placeholder="Ingrese la latitud " class="form-control" required  name="latitud_su" value="<?php echo $sucursalEditar->latitud_su; ?>" readonly id="latitud_su">
        </div>
        <div class="col-md-6">
          <label for="">Longitud de la sucursal</label>
          <input type="text" placeholder="Ingrese la longitud " class="form-control" required name="longitud_su" value="<?php echo $sucursalEditar->longitud_su; ?>" readonly id="longitud_su">
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <div id="mapaUbicacion" style="height:300px; width:100%; border:2px solid black;"></div>
        </div>
      </div>
      <script type="text/javascript">
        function initMap(){
          var centro=new google.maps.LatLng(<?php echo $sucursalEditar->latitud_su; ?>, <?php echo $sucursalEditar->longitud_su; ?>);
          var mapa1=new google.maps.Map(
            document.getElementById('mapaUbicacion'),
            {
              center:centro,
              zoom:7,
              mapTypeId:google.maps.MapTypeId.ROADMAP
            }
          );

          var marcador=new google.maps.Marker(
            {
              position:centro,
              map:mapa1,
              title:"Seleccione la dirección",
              icon:"<?php echo base_url() ?>/assets/images/su.png",
              draggable:true
            }
          );
          google.maps.event.addListener(marcador,'dragend',function(){
            // alert("Se termino el drag");
            document.getElementById('latitud_su').value=this.getPosition().lat();
            document.getElementById('longitud_su').value=this.getPosition().lng();
          });
        }//cierre de la funcion initMap
      </script>
      <br><br>
      <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('sucursales/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>
      </div>
    </div>
      </div>
      <br><br>
    </div
  </form>
</div>
