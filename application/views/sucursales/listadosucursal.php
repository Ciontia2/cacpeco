<br><br>
<style>
  .contenedor{
    box-shadow: 0px 0px 5px 1px black;
  }
</style>
<div class="container contenedor">
  <br>
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h1 class="text-center">LISTADO DE SUCURSALES</h1>
      </div>
      <div class="col-md-4 nuevo">
        <a href="<?php echo site_url(); ?>/sucursales/nuevosucursal" class="btn btn-primary">Agregar Sucursal</a>
      </div>
    </div>
  </div>
  <div class="table-responsive">
    <?php if ($sucursales): ?>
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>NOMBRE SUCURSAL</th>
            <th>TELEFONO</th>
            <th>GERENTE</th>
            <th>FECHA APERTURA</th>
            <th>FOTO</th>
            <th>LATITUD DE LA SUCURSAL</th>
            <th>LONGITUD DE LA SUCURSAL</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($sucursales as $filaTemporal): ?>
            <tr>
              <td>
                <?php echo $filaTemporal->id_su ?>
              </td>
              <td>
                <?php echo $filaTemporal->nombre_su ?>
              </td>
              <td>
                <?php echo $filaTemporal->telefono_su ?>
              </td>
              <td>
                <?php echo $filaTemporal-> gerente_su?>
              </td>
              <td>
                <?php echo $filaTemporal->fechaapertura_su ?>
              </td>
              <td>
                <?php echo $filaTemporal-> foto_su?>
              </td>
              <td>
                <?php echo $filaTemporal-> latitud_su?>
              </td>
              <td>
                <?php echo $filaTemporal-> longitud_su?>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url(); ?>/sucursales/editarsucursal/<?php echo $filaTemporal->id_su ?>" title="Editar Sucursal" style="color:blue;">
                    <i class="bi bi-pencil-square"></i>
                </a>
                  &nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/sucursales/eliminar/<?php echo $filaTemporal->id_su ?>"title="Eliminar Sucursal"> <i class="fa fa-trash text-primary"></i></a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <br>
    <?php else: ?>
      <h1 class="text-center">No existen Sucursales registradas</h1>
    <?php endif; ?>
  </div>
</div>
