<br>
<style>
  .contenedor{
    box-shadow: 0px 0px 5px 1px black;
  }
</style>
<div class="container contenedor">
  <br>
  <h1 class="text-center">NUEVA SUCURSAL</h1>
  <br>
  <form class="" action="<?php echo site_url(); ?>/sucursales/guardarSucursal" method="post" enctype="multipart/form-data">
    <div class="container">
      <div class="row">
          <div class="col-md-6">
            <label for="">Nombre sucursal</label>
            <br>
            <input type="text" placeholder="Ingrese el nombre de la sucursal" class="form-control" required  name="nombre_su" value="" id="nombre_su">
          </div>
          <div class="col-md-6">
            <label for="">Telefono</label>
            <br>
            <input type="number" placeholder="Ingrese el telefono" class="form-control" required  name="telefono_su" value="" id="telefono_su">
          </div>
      </div>
      <br>
      <div class="row">
          <div class="col-md-6">
            <label for="">Gerente</label>
            <br>
            <input type="text" placeholder="Ingrese los nombres del gerente" class="form-control" required  name="gerente_su" value="" id="gerente_su">
          </div>
          <div class="col-md-6">
            <label for="">Fecha de apertura</label>
            <br>
            <input type="date" placeholder="Ingrese la fecha de apertura" class="form-control" required  name="fechaapertura_su" value="" id="fechaapertura_su">
          </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <label for="foto_su" style="display: block; margin-bottom: 10px;"><b>Foto:</b></label>
          <input type="file" name="foto_su" id="foto_su" value="" required accept="image/*" style="width: 10%; padding: 8px;">
          <br>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-6">
          <label for="">Latitud de la sucursal</label>
          <input type="text" placeholder="Ingrese la latitud" class="form-control" required  name="latitud_su" value="" readonly id="latitud_su">
        </div>
        <div class="col-md-6">
          <label for="">Longitud de la sucursal</label>
          <input type="text" placeholder="Ingrese la longitud" class="form-control" required  name="longitud_su" value="" readonly id="longitud_su">
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <div id="mapaUbicacion" style="height:300px; width:100%; border:2px solid black;"></div>
        </div>
      </div>
      <script type="text/javascript">
        function initMap(){
          var centro=new google.maps.LatLng(-1.2595931997473242, -78.54164276317397);
          var mapa1=new google.maps.Map(
            document.getElementById('mapaUbicacion'),
            {
              center:centro,
              zoom:7,
              mapTypeId:google.maps.MapTypeId.ROADMAP
            }
          );

          var marcador=new google.maps.Marker(
            {
              position:centro,
              map:mapa1,
              title:"Seleccione la dirección",
              icon:"<?php echo base_url() ?>/assets/images/su.png",
              draggable:true
            }
          );
          google.maps.event.addListener(marcador,'dragend',function(){
            // alert("Se termino el drag");
            document.getElementById('latitud_su').value=this.getPosition().lat();
            document.getElementById('longitud_su').value=this.getPosition().lng();
          });
        }//cierre de la funcion initMap
      </script>
      <br><br>
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
        &nbsp
        <a href="<?php echo site_url(); ?>/sucursales/index" class="btn btn-danger">CANCELAR</a>
           </div>
      </div>
      <br><br>
    </div
  </form>
</div>
