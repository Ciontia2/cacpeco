<div class="row">
  <div class="col-md-12">
    <div id="mapaUbicacion" style="height: 700px; width: 100%; border: 2px solid black;"></div>
  </div>
</div>

<script type="text/javascript">
  function initMap() {
    var centro = new google.maps.LatLng(-1.2595931997473242, -78.54164276317397);
    var mapa1 = new google.maps.Map(
      document.getElementById('mapaUbicacion'),
      {
        center: centro,
        zoom: 7,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
    );

    // Supongamos que tienes un array llamado 'marcadores' con información de sucursales, cajeros y corresponsales
    var marcadores = <?php echo json_encode(['sucursales' => $sucursales, 'cajeros' => $cajeros, 'corresponsales' => $corresponsales]); ?>;

    // Agregar marcadores de sucursales
    for (var i = 0; i < marcadores.sucursales.length; i++) {
      var sucursal = marcadores.sucursales[i];
      agregarMarcador(mapa1, sucursal.latitud_su, sucursal.longitud_su, 'Sucursal: ' + sucursal.nombre_su, 'su.png');
    }

    // Agregar marcadores de cajeros
    for (var i = 0; i < marcadores.cajeros.length; i++) {
      var cajero = marcadores.cajeros[i];
      agregarMarcador(mapa1, cajero.latitud_ca, cajero.longitud_ca, 'Cajero ID: ' + cajero.estado_ca, 'ca.png');
    }

    // Agregar marcadores de corresponsales
    for (var i = 0; i < marcadores.corresponsales.length; i++) {
      var corresponsal = marcadores.corresponsales[i];
      agregarMarcador(mapa1, corresponsal.latitud_co, corresponsal.longitud_co, 'Corresponsal ID: ' + corresponsal.nombre_su, 'co.png');
    }
  }

  function agregarMarcador(mapa, latitud, longitud, titulo, icono) {
    var marcador = new google.maps.Marker({
      position: new google.maps.LatLng(latitud, longitud),
      map: mapa,
      title: titulo,
      icon: "<?php echo base_url() ?>/assets/images/" + icono,
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function () {
      document.getElementById('latitud_su').value = this.getPosition().lat();
      document.getElementById('longitud_su').value = this.getPosition().lng();
    });
  }

  // Llama a la función initMap() después de que la página esté completamente cargada
  document.addEventListener('DOMContentLoaded', initMap);
</script>
