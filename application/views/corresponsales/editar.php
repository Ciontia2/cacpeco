<br>
<style>
  .contenedor{
    box-shadow: 0px 0px 5px 1px black;
  }
</style>
<div class="container contenedor">
  <br>
  <h1 class="text-center">ACTUALIZAR DATOS DE CORRESPONSAL</h1>
  <br>
  <form class="" action="<?php echo site_url(); ?>/corresponsales/actualizarCorresponsal" method="post"enctype="multipart/form-data">
    <div class="container">
      <div class="row">
          <div class="col-md-6">
            <input type="hidden" name="id_co" id="id_co" value="<?php echo $corresponsalEditar->id_co; ?>">
            <label for="">NOMBRE</label>
            <br>
            <input type="text" placeholder="Ingrese nombre" class="form-control" required name="nombre_co" value="<?php echo $corresponsalEditar->nombre_co; ?>" id="nombre_co">
          </div>
          <div class="col-md-6">
            <label for="">DIRECCION</label>
            <br>
            <input type="text" placeholder="Ingrese la direccion" class="form-control" required  name="direccion_co" value="<?php echo $corresponsalEditar->direccion_co; ?>" id="direccion_co">
          </div>
      </div>
      <br>
      <div class="row">
          <div class="col-md-6">
            <label for="">Tipo servicio</label>
            <br>
            <input type="text" placeholder="Ingrese el tipo de srvicio" class="form-control" required  name="tipo_servicio_co" value="<?php echo $corresponsalEditar->tipo_servicio_co; ?>" id="tipo_servicio_co">
          </div>
          <div class="col-md-6">
            <label for="">Comicion</label>
            <br>
            <input type="text" placeholder="Ingrese la comicion" class="form-control" required  name="comision_co" value="<?php echo $corresponsalEditar->comision_co; ?>" id="comision_co">
          </div>
      </div>
      <br>
      <br>
      <div class="row">
        <div class="col-md-6">
          <label for="">Latitud de la Corresponsal</label>
          <input type="text" placeholder="Ingrese la latitud " class="form-control" required  name="latitud_co" value="<?php echo $corresponsalEditar->latitud_co; ?>"  readonly id="latitud_co">
        </div>
        <div class="col-md-6">
          <label for="">Longitud de la Corresponsal</label>
          <input type="text" placeholder="Ingrese la longitud " class="form-control" required  name="longitud_co" value="<?php echo $corresponsalEditar->longitud_co; ?>"  readonly id="longitud_co">
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <div id="mapaUbicacion" style="height:300px; width:100%; border:2px solid black;"></div>
        </div>
      </div>
      <script type="text/javascript">
        function initMap(){
          var centro=new google.maps.LatLng(<?php echo $corresponsalEditar->latitud_co; ?>, <?php echo $corresponsalEditar->longitud_co; ?>);
          var mapa1=new google.maps.Map(
            document.getElementById('mapaUbicacion'),
            {
              center:centro,
              zoom:7,
              mapTypeId:google.maps.MapTypeId.ROADMAP
            }
          );

          var marcador=new google.maps.Marker(
            {
              position:centro,
              map:mapa1,
              title:"Seleccione la dirección",
                icon:"<?php echo base_url() ?>/assets/images/co.png",
              draggable:true
            }
          );
          google.maps.event.addListener(marcador,'dragend',function(){
            // alert("Se termino el drag");
            document.getElementById('latitud_co').value=this.getPosition().lat();
            document.getElementById('longitud_co').value=this.getPosition().lng();
          });
        }//cierre de la funcion initMap
      </script>
      <br><br>
      <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('corresponsales/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>
      </div>
    </div>
      </div>
      <br><br>
    </div
  </form>
</div>
