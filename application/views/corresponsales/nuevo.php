<br>
<style>
  .contenedor{
    box-shadow: 0px 0px 5px 1px black;
  }
</style>
<div class="container contenedor">
  <br>
  <h1 class="text-center">NUEVA CORRESPONSAL</h1>
  <br>
  <form class="" action="<?php echo site_url(); ?>/corresponsales/guardarCorresponsal" method="post" enctype="multipart/form-data" id="form-corresponsal">
    <div class="container">
      <div class="row">
          <div class="col-md-6">
            <label for="">Nombre Corresponsal</label>
            <br>
            <input type="text" placeholder="Ingrese el nombre de la corresponsal" class="form-control" required  name="nombre_co" value="" id="nombre_co" required>
          </div>
          <div class="col-md-6">
            <label for="">Direccion</label>
            <br>
            <input type="text" placeholder="Ingrese la direccion" class="form-control" required  name="direccion_co" value="" id="direccion_co" required>
          </div>
      </div>
      <br>
      <div class="row">
          <div class="col-md-6">
            <label for="">Tipo Servicio</label>
            <br>
            <input type="text" placeholder="Ingrese el tipo de servisio" class="form-control" required  name="tipo_servicio_co" value="" id="tipo_servicio_co"required>
          </div>
          <div class="col-md-6">
            <label for="">Comicion</label>
            <br>
            <input type="number" placeholder="Ingrese la comicion" class="form-control" required  name="comision_co" value="" id="comision_co"required>
          </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-6">
          <label for="">Latitud de la corresponsal</label>
          <input type="text" placeholder="Ingrese la latitud" class="form-control" required  name="latitud_co" value=""  readonly id="latitud_co"required>
        </div>
        <div class="col-md-6">
          <label for="">Longitud de la corresponsal</label>
          <input type="text" placeholder="Ingrese la longitud" class="form-control" required name="longitud_co" value=""  readonly id="longitud_co"required>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <div id="mapaUbicacion" style="height:300px; width:100%; border:2px solid black;"></div>
        </div>
      </div>
      <script type="text/javascript">
        function initMap(){
          var centro=new google.maps.LatLng(-1.2595931997473242, -78.54164276317397);
          var mapa1=new google.maps.Map(
            document.getElementById('mapaUbicacion'),
            {
              center:centro,
              zoom:7,
              mapTypeId:google.maps.MapTypeId.ROADMAP
            }
          );

          var marcador=new google.maps.Marker(
            {
              position:centro,
              map:mapa1,
              title:"Seleccione la dirección",
              icon:"<?php echo base_url() ?>/assets/images/co.png",
              draggable:true
            }
          );
          google.maps.event.addListener(marcador,'dragend',function(){
            // alert("Se termino el drag");
            document.getElementById('latitud_co').value=this.getPosition().lat();
            document.getElementById('longitud_co').value=this.getPosition().lng();
          });
        }//cierre de la funcion initMap
      </script>
      <br><br>
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
        &nbsp
        <a href="<?php echo site_url(); ?>/corresponsales/index" class="btn btn-danger">CANCELAR</a>
           </div>
      </div>
      <br><br>
    </div>
  </form>
</div>
<!-- Incluye jQuery desde la carpeta de activos -->
    <script src="<?php echo base_url('assets/librerias/jquery-validate'); ?>"></script>
    <!-- Incluye jQuery Validate desde la carpeta de activos -->
    <script src="<?php echo base_url('assets/librerias/jquery.validate.min.js'); ?>"></script>
    <script>
        $(document).ready(function () {
            // Inicializa la validación del formulario
            $('#form-corresponsal').validate({
                rules: {
                    // Definir reglas de validación para cada campo
                    nombre_co: {
                        required: true
                    },
                    direccion_co: {
                        required: true
                    },
                    tipo_servicio_co: {
                        required: true
                    },
                    comision_co: {
                        required: true,
                        number: true
                    },
                    latitud_co: {
                        required: true
                    },
                    longitud_co: {
                        required: true
                    }
                },
                messages: {
                    // Mensajes de error personalizados
                    nombre_co: {
                        required: "Por favor ingrese un nombre"
                    },
                    direccion_co: {
                        required: "Por favor ingrese la dirección"
                    },
                    tipo_servicio_co: {
                        required: "Por favor ingrese el tipo de servicio"
                    },
                    comision_co: {
                        required: "Por favor, ingrese la comisión",
                        number: "Por favor, ingrese un número válido"
                    },
                    latitud_co: {
                        required: "Por favor, ingrese la latitud"
                    },
                    longitud_co: {
                        required: "Por favor, ingrese la longitud"
                    }
                },
                submitHandler: function (form) {
                    // Lógica adicional antes de enviar el formulario
                    form.submit();
                }
            });
        });
    </script>
