
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>CACPECO</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/cacpeco.png" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@500;600;700&family=Open+Sans:wght@400;500&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?php echo base_url(); ?>assets/plantilla/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plantilla/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plantilla/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo base_url(); ?>assets/plantilla/css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="<?php echo base_url(); ?>assets/plantilla/css/style.css" rel="stylesheet">

    <!-- Importacion del API KEY DE GOOGLE -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbsAG6qvUo-3N-P9_os86U4cqcpODujq0&libraries=places&callback=initMap">
    </script>

    <!-- Importacion de sweetalert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.all.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.min.css">

</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner-border text-primary" role="status" style="width: 3rem; height: 3rem;"></div>
    </div>
    <!-- Spinner End -->
    <!-- Navbar Start -->
    <nav class="navbar navbar-expand-lg bg-white navbar-light sticky-top p-0">
        <a href="<?php echo site_url() ?>" class="navbar-brand d-flex align-items-center px-4 px-lg-5">
          <img width="300px" src="<?php echo base_url()?>/assets/images/im.png" alt="">
        </a>
        <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto p-4 p-lg-0">
                <a href="<?php echo site_url() ?>" class="nav-item nav-link">INICIO</a>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">SUCURSAL</a>
                    <div class="dropdown-menu bg-light m-0">
                        <a href="<?php echo site_url(); ?>/sucursales/nuevo" class="dropdown-item">Agregar Sucursal  </a>
                        <a href="<?php echo site_url(); ?>/sucursales/index" class="dropdown-item">Listado Sucursales</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">CAJEROS</a>
                    <div class="dropdown-menu bg-light m-0">
                        <a href="<?php echo site_url(); ?>/cajeros/nuevo" class="dropdown-item">Agregar Cajero </a>
                        <a href="<?php echo site_url(); ?>/cajeros/index" class="dropdown-item">Listado Cajeros</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">CORRESPONSALES</a>
                    <div class="dropdown-menu bg-light m-0">
                        <a href="<?php echo site_url(); ?>/corresponsales/nuevo" class="dropdown-item">Agregar Corresponsal </a>
                        <a href="<?php echo site_url(); ?>/corresponsales/index" class="dropdown-item">Listado Corresponsales</a>
                    </div>
                </div>
            <a href="<?php echo site_url(); ?>/ubicaciones/ubicaciones_general" class="btn btn-primary py-4 px-lg-4 rounded-0 d-none d-lg-block">REPORTE GENERAL<i class="fa fa-arrow-right ms-3"></i></a>
        </div>
    </nav>
    <!-- Navbar End -->
    <?php if ($this->session->flashdata('confirmacion')): ?>
        <script type="text/javascript">
        Swal.fire({
        title: "Confirmacion",
        text: "<?php echo $this->session->flashdata('confirmacion'); ?>",
        icon: "success"
      });
    </script>
    <?php $this->session->set_flashdata('confirmacion',''); ?>
  <?php endif; ?>
