<br>
<style>
  .contenedor{
    box-shadow: 0px 0px 5px 1px black;
  }
</style>
<div class="container contenedor">
  <br>
  <h1 class="text-center">NUEVO CAJERO</h1>
  <br>
  <form class="" action="<?php echo site_url(); ?>/cajeros/guardarCajero" method="post" enctype="multipart/form-data">
    <div class="container">
      <div class="row">
          <div class="col-md-6">
            <label for="">ESTADO</label>
            <br>
            <input type="text" placeholder="Ingrese el estado" class="form-control" required  name="estado_ca" value="" id="estado_ca">
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <label for="foto_ca" style="display: block; margin-bottom: 10px;"><b>Foto:</b></label>
              <input type="file" name="foto_ca" id="foto_ca" value="" required accept="image/*" style="width: 10%; padding: 8px;">
            </div>
          </div>
          <div class="col-md-6">
            <label for="">SALDO ACTUAL</label>
            <br>
            <input type="number" placeholder="Ingrese el saldo actual" class="form-control" required  name="saldo_actual_ca" value="" id="saldo_actual_ca">
          </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-6">
          <label for="">Latitud cajero</label>
          <input type="text" placeholder="Ingrese la latitud" class="form-control" required   name="latitud_ca" value="" id="latitud_ca" readonly>
        </div>
        <div class="col-md-6">
          <label for="">Longitud cajero</label>
          <input type="text" placeholder="Ingrese la longitud" class="form-control" required  name="longitud_ca" value="" id="longitud_ca" readonly>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <div id="mapaUbicacion" style="height:300px; width:100%; border:2px solid black;"></div>
        </div>
      </div>
      <script type="text/javascript">
        function initMap(){
          var centro=new google.maps.LatLng(-1.2595931997473242, -78.54164276317397);
          var mapa1=new google.maps.Map(
            document.getElementById('mapaUbicacion'),
            {
              center:centro,
              zoom:7,
              mapTypeId:google.maps.MapTypeId.ROADMAP
            }
          );

          var marcador=new google.maps.Marker(
            {
              position:centro,
              map:mapa1,
              title:"Seleccione la dirección",
              icon:"<?php echo base_url() ?>/assets/images/ca.png",
              draggable:true
            }
          );
          google.maps.event.addListener(marcador,'dragend',function(){
            // alert("Se termino el drag");
            document.getElementById('latitud_ca').value=this.getPosition().lat();
            document.getElementById('longitud_ca').value=this.getPosition().lng();
          });
        }//cierre de la funcion initMap
      </script>
      <br><br>
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
        &nbsp
        <a href="<?php echo site_url(); ?>/cajeros/index" class="btn btn-danger">CANCELAR</a>
           </div>
      </div>
      <br><br>
    </div
  </form>
</div>
