<br>
<style>
  .contenedor{
    box-shadow: 0px 0px 5px 1px black;
  }
</style>
<div class="container contenedor">
  <br>
  <h1 class="text-center">ACTUALIZAR DATOS DE CAJEROS</h1>
  <br>
  <form class="" action="<?php echo site_url(); ?>/cajeros/actualizarCajero" method="post"enctype="multipart/form-data">
    <div class="container">
      <div class="row">
          <div class="col-md-6">
            <input type="hidden" name="id_ca" id="id_ca" value="<?php echo $cajeroEditar->id_ca; ?>">
            <label for="">ESTADO</label>
            <br>
            <input type="text" placeholder="Ingrese estado" class="form-control"required name="estado_ca" value="<?php echo $cajeroEditar->estado_ca; ?>" id="estado_ca">
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <label for="foto_ca" style="display: block; margin-bottom: 10px;"><b>FOTO:</b></label>
              <input type="file" name="foto_ca" id="foto_ca" value="" required accept="image/*"  value="<?php echo $cajeroEditar->foto_ca; ?>"style="width: 100%; padding: 8px;">
                   <?php if (!empty($cajeroEditar->foto_ca)): ?>
                      <br>
                      <img src="<?php echo base_url('uploads/cajeros/') . $cajeroEditar->foto_ca; ?>" height="100px" alt="foto actual">
                   <?php endif; ?>
                   <br>
            </div>
          </div>
          <div class="col-md-6">
            <label for="">SALDO ACTUAL</label>
            <br>
            <input type="number" placeholder="Ingrese el saldo_actual" required class="form-control" name="saldo_actual_ca" value="<?php echo $cajeroEditar->saldo_actual_ca; ?>" id="saldo_actual_ca">
          </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-6">
          <label for="">Latitud de cajeros</label>
          <input type="text" placeholder="Ingrese la latitud " class="form-control"  readonly required name="latitud_ca" value="<?php echo $cajeroEditar->latitud_ca; ?>" id="latitud_ca">
        </div>
        <div class="col-md-6">
          <label for="">Longitud de cajeros</label>
          <input type="text" placeholder="Ingrese la longitud " class="form-control"  readonly required  name="longitud_ca" value="<?php echo $cajeroEditar->longitud_ca; ?>" id="longitud_ca">
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <div id="mapaUbicacion" style="height:300px; width:100%; border:2px solid black;"></div>
        </div>
      </div>
      <script type="text/javascript">
        function initMap(){
          var centro=new google.maps.LatLng(<?php echo $cajeroEditar->latitud_ca; ?>, <?php echo $cajeroEditar->longitud_ca; ?>);
          var mapa1=new google.maps.Map(
            document.getElementById('mapaUbicacion'),
            {
              center:centro,
              zoom:7,
              mapTypeId:google.maps.MapTypeId.ROADMAP
            }
          );

          var marcador=new google.maps.Marker(
            {
              position:centro,
              map:mapa1,
              title:"Seleccione la dirección",
              icon:"<?php echo base_url() ?>/assets/images/ca.png",
              draggable:true
            }
          );
          google.maps.event.addListener(marcador,'dragend',function(){
            // alert("Se termino el drag");
            document.getElementById('latitud_ca').value=this.getPosition().lat();
            document.getElementById('longitud_ca').value=this.getPosition().lng();
          });
        }//cierre de la funcion initMap
      </script>
      <br><br>
      <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>
      </div>
    </div>
      </div>
      <br><br>
    </div
  </form>
</div>
