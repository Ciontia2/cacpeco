<br><br>
<style>
  .contenedor{
    box-shadow: 0px 0px 5px 1px black;
  }
</style>
<div class="container contenedor">
  <br>
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h1 class="text-center">LISTADO DE CAJEROS</h1>
      </div>
      <div class="col-md-2 nuevo">
      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i> Ver Mapa
      </button>
      </div>
      <div class="col-md-2 nuevo">
        <a href="<?php echo site_url('cajeros/nuevo'); ?>" class="btn btn-outline-success">
            <i class="fa fa-plus-circle fa-1x" ></i>  Agregar Cajeros
            </a>
      </div>
    </div>
  </div>
  <br>
  <br>
  <div class="table-responsive">
    <?php if ($listadoCajeros): ?>
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>ESTADO</th>
            <th>FOTO</th>
            <th>SALDO ACTUAL</th>
            <th>LATITUD DEL CAJERO</th>
            <th>LONGITUD DEL CAJERO</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoCajeros as $filaTemporal): ?>
            <tr>
              <td>
                <?php echo $filaTemporal->id_ca ?>
              </td>
              <td>
                <?php echo $filaTemporal->estado_ca ?>
              </td>
              <td>
                  <?php if ($filaTemporal->foto_ca!=""): ?>
                    <img src="<?php echo base_url('uploads/cajeros/') . $filaTemporal->foto_ca; ?>" width="100px" height="100px" alt="">
                  <?php else: ?>
                    N/A
                  <?php endif; ?>
                </td>
              <td>
                <?php echo $filaTemporal->saldo_actual_ca ?>
              </td>
              <td>
                <?php echo $filaTemporal-> latitud_ca?>
              </td>
              <td>
                <?php echo $filaTemporal-> longitud_ca?>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url(); ?>/cajeros/editar/<?php echo $filaTemporal->id_ca ?>" title="Editar Cajeros" style="color:blue;">
                    <i class="bi bi-pencil-square"></i>
                </a>
                  &nbsp;&nbsp;&nbsp;
                  <a href="<?php echo site_url(); ?>/cajeros/borrar/<?php echo $filaTemporal->id_ca ?>" title="Eliminar Cajeros">
                      <i class="fa fa-trash text-danger"></i>
                  </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <br>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <i class="fa fa-eye"></i>Mapa de Cajeros
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="reporteMapa"
          style="height: 300px; width:100%; border:2px solid black;">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary"
          data-bs-dismiss="modal"> <i class="fa fa-times"></i>Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
      function initMap(){
        var coordenadaCentral=
            new google.maps.LatLng(-0.152948869329262,
              -78.4868431364856);
        var miMapa=new google.maps.Map(
          document.getElementById('reporteMapa'),
          {
            center:coordenadaCentral,
            zoom:8,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
        );
        <?php foreach ($listadoCajeros as $filaTemporal): ?>
        var coordenadaTemporal=
            new google.maps.LatLng(
              <?php echo $filaTemporal->latitud_ca; ?>,
              <?php echo $filaTemporal->longitud_ca; ?>);
          var marcador=new google.maps.Marker({
            position:coordenadaTemporal,
            map:miMapa,
            title:'<?php echo $filaTemporal->estado_ca; ?>',
            icon:"<?php echo base_url() ?>/assets/images/ca.png",
          });
        <?php endforeach; ?>

      }
    </script>
    <?php else: ?>
      <h1 class="text-center">No existen Cajeros registrados</h1>
    <?php endif; ?>
  </div>
</div>
