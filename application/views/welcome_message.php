<!-- Carousel Start -->
  <br><br>
   <div class="container-fluid p-0 wow fadeIn" data-wow-delay="0.1s">
       <div id="header-carousel" class="carousel slide" data-bs-ride="carousel">
           <div class="carousel-inner">
               <div class="carousel-item active">
                   <img class="w-100" src="<?php echo base_url() ?>/assets/images/3.jpg" alt="Image" width="500" height="500">
               </div>
               <div class="carousel-item">
                   <img class="w-100" src="<?php echo base_url() ?>/assets/images/5.jpg" alt="Image" width="500" height="500">
               </div>
               <div class="carousel-item">
                   <img class="w-100" src="<?php echo base_url() ?>/assets/images/4.jpg" alt="Image" width="500" height="500">
               </div>
           </div>
           <button class="carousel-control-prev" type="button" data-bs-target="#header-carousel"
               data-bs-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="visually-hidden">Previous</span>
           </button>
           <button class="carousel-control-next" type="button" data-bs-target="#header-carousel"
               data-bs-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="visually-hidden">Next</span>
           </button>
       </div>
   </div>
   <br> <br> <br>
   <div class="row">
    <!-- Sección de Visión -->
    <section class="col-md-6 border border-blue p-4" style="text-align: justify; color: black;">
        <h2>Visión</h2>
        <p>Entregamos productos y servicios solidarios de excelencia, con gestión sostenible apoyados en tecnología vanguardista y talento humano comprometido.</p>
    </section>

    <!-- Sección de Misión -->
    <section class="col-md-6 border border-blue p-4" style="text-align: justify; color: black;">
        <h2>Misión</h2>
        <p>CACPECO será reconocida como una Cooperativa Sostenible, que consolide relaciones de confianza con sus socios, en respaldo a sus proyectos de vida; con un servicio innovador, seguro y dinámico.</p>
    </section>
</div>


   <!-- Carousel End -->
   <br><br><br><br>
   <!-- Top Feature Start -->
    <div class="container-fluid top-feature py-5 pt-lg-0">
        <div class="container py-5 pt-lg-0">
            <div class="row gx-0">
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.1s">
                    <div class="bg-white shadow d-flex align-items-center h-100 px-5" style="min-height: 160px;">
                        <div class="d-flex">
                            <div class="flex-shrink-0 btn-lg-square rounded-circle bg-light">
                                <i class="fa fa-plus text-primary"></i>
                            </div>
                            <div class="ps-3">
                                <a href="<?php echo site_url(); ?>/sucursales/nuevo"><h4>AGREGAR UNA NUEVA SUCURSAL</h4></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top Feature End -->
